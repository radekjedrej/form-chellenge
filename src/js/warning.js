// ##############
// Script For At Least One Checkbox To Be Picked 3 Groups
// ##############

$(function(){
  var requiredCheckboxes = $('.groupa :checkbox[required]');
  requiredCheckboxes.change(function(){
      if(requiredCheckboxes.is(':checked')) {
          requiredCheckboxes.removeAttr('required');
      } else {
          requiredCheckboxes.attr('required', 'required');
      }
  });

  var requiredCheckboxes2 = $('.groupb :checkbox[required]');
  requiredCheckboxes2.change(function(){
      if(requiredCheckboxes2.is(':checked')) {
          requiredCheckboxes2.removeAttr('required');
      } else {
          requiredCheckboxes2.attr('required', 'required');
      }
  });

  var requiredCheckboxes3 = $('.groupc :checkbox[required]');
  requiredCheckboxes3.change(function(){
      if(requiredCheckboxes3.is(':checked')) {
          requiredCheckboxes3.removeAttr('required');
      } else {
          requiredCheckboxes3.attr('required', 'required');
      }
  });


});