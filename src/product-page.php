<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="x-ua-compatible" content="ie=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <link rel="stylesheet" href="css/style.css">
        <title></title>
    </head>
<body>
  
  <section class="product-page">
    <div class="container product-page-container">
        <div class="back-div">
          <h4><span><</span> <a href="index.php" class="back-button">BACK</a></h4>  
        </div>
        <div class="main-logo text-center">
            <img class="img-fluid" src="img/main/main-logo.jpg.png" alt="Glam Tidings">
            <div class="heading-text text-center">
              <h4>STUCK? USE OUR <a class="purple" href="#">GIFT FINDER!</a> <span class="caret-black purple">></span></h4>
            </div>
        </div>

        <div class="row protuct-page-row">

          <!-- // ##############
          // Product 1
          // ############## -->
          <div class="col-6 col-md-3 product-wrapper">
            <div class="image-wrapper text-center">
              <a data-toggle="modal" data-target=".product-1" href="#"><img class="img-fluid" src="img/products/product-1.png" alt="Try your luxe"></a>
            </div>
            <div class="text-wrapper text-center text-uppercase">
              <div class="title ">
                try your lux
              </div>
              <div class="view-more">
                <a data-toggle="modal" data-target=".product-1" href="#">view more details <span class="caret-black">></span></a>
              </div>
              <div class="add-to-basket">
                <p>add to basket <span class="caret-black">></span></p> 
              </div>
            </div>
          </div>

          <!-- Modal Content: begins -->
          <div class="modal fade product-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">                       
              <div class="modal-content">
                
                <!-- Modal Header -->
                <div class="modal-header no-borders">
                    <button type="button" class="close" data-dismiss="modal"><img src="img/main/close-button.png" alt=""></button>
                    <h4 class="modal-title" id="gridSystemModalLabel"></h4>
                </div>   
                <!-- Modal Body -->  
                <div class="modal-body text-center">
                  <h2 class="text-uppercase">TRY YOUR LUXE</h2>
                  <p class="body-message centered"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore officiis vitae aperiam esse perferendis iusto alias distinctio provident id incidunt. Eaque rerum illum quia eum tenetur molestias? Accusantium iste est fugiat quasi numquam quibusdam vero! Quibusdam facere placeat quisquam repudiandae. </p>
                  <span>£12.00</span>
                </div>

              </div>                           
            </div>              
          </div><!-- Modal Content: ends -->

           <!-- // ##############
          // Product 2
          // ############## -->
          <div class="col-6 col-md-3 product-wrapper">
              <div class="image-wrapper">
                <a data-toggle="modal" data-target=".product-2" href="#"><img class="img-fluid" src="img/products/product-2.png" alt="Try your luxe"></a>
              </div>
              <div class="text-wrapper text-center text-uppercase">
                <div class="title ">
                  beauty-full house
                </div>
                <div class="view-more">
                  <a data-toggle="modal" data-target=".product-2" href="#">view more details <span class="caret-black">></span></a>
                </div>
                <div class="add-to-basket">
                  <p>add to basket <span class="caret-black">></span></p> 
                </div>
              </div>
            </div>
  
            <!-- Modal Content: begins -->
            <div class="modal fade product-2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                                 
                  <div class="modal-content">
                    
                    <!-- Modal Header -->
                    <div class="modal-header no-borders">
                        <button type="button" class="close" data-dismiss="modal"><img src="img/main/close-button.png" alt=""></button>
                        <h4 class="modal-title" id="gridSystemModalLabel"></h4>
                    </div>   
                    <!-- Modal Body -->  
                    <div class="modal-body text-center">
                      <h2 class="text-uppercase">beauty-full house</h2>
                      <p class="body-message centered"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore officiis vitae aperiam esse perferendis iusto alias distinctio provident id incidunt. Eaque rerum illum quia eum tenetur molestias? Accusantium iste est fugiat quasi numquam quibusdam vero! Quibusdam facere placeat quisquam repudiandae. </p>
                      <span>£12.00</span>
                    </div>
  
                  </div>                           
                </div>              
              </div><!-- Modal Content: ends -->

          <!-- // ##############
          // Product 3
          // ############## -->
          <div class="col-6 col-md-3 product-wrapper">
              <div class="image-wrapper">
                <a data-toggle="modal" data-target=".product-3" href="#"><img class="img-fluid" src="img/products/product-3.png" alt="Try your luxe"></a>
              </div>
              <div class="text-wrapper text-center text-uppercase">
                <div class="title ">
                    Extrava-glamza
                </div>
                <div class="view-more">
                  <a data-toggle="modal" data-target=".product-3" href="#">view more details <span class="caret-black">></span></a>
                </div>
                <div class="add-to-basket">
                  <p>add to basket <span class="caret-black">></span></p> 
                </div>
              </div>
            </div>
  
            <!-- Modal Content: begins -->
            <div class="modal fade product-3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                                 
                  <div class="modal-content">
                    
                    <!-- Modal Header -->
                    <div class="modal-header no-borders">
                        <button type="button" class="close" data-dismiss="modal"><img src="img/main/close-button.png" alt=""></button>
                        <h4 class="modal-title" id="gridSystemModalLabel"></h4>
                    </div>   
                    <!-- Modal Body -->  
                    <div class="modal-body text-center">
                      <h2 class="text-uppercase">Extrava-glamza</h2>
                      <p class="body-message centered"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore officiis vitae aperiam esse perferendis iusto alias distinctio provident id incidunt. Eaque rerum illum quia eum tenetur molestias? Accusantium iste est fugiat quasi numquam quibusdam vero! Quibusdam facere placeat quisquam repudiandae. </p>
                      <span>£12.00</span>
                    </div>
  
                  </div>                           
                </div>              
              </div><!-- Modal Content: ends -->
        
          <!-- // ##############
          // Product 4
          // ############## -->
          <div class="col-6 col-md-3 product-wrapper">
              <div class="image-wrapper">
                <a data-toggle="modal" data-target=".product-4" href="#"><img class="img-fluid" src="img/products/product-4.png" alt="Try your luxe"></a>
              </div>
              <div class="text-wrapper text-center text-uppercase">
                <div class="title ">
                  hello super spa
                </div>
                <div class="view-more">
                  <a data-toggle="modal" data-target=".product-4" href="#">view more details <span class="caret-black">></span></a>
                </div>
                <div class="add-to-basket">
                  <p>add to basket <span class="caret-black">></span></p> 
                </div>
              </div>
            </div>
  
            <!-- Modal Content: begins -->
            <div class="modal fade product-4" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                                 
                  <div class="modal-content">
                    
                    <!-- Modal Header -->
                    <div class="modal-header no-borders">
                        <button type="button" class="close" data-dismiss="modal"><img src="img/main/close-button.png" alt=""></button>
                        <h4 class="modal-title" id="gridSystemModalLabel"></h4>
                    </div>   
                    <!-- Modal Body -->  
                    <div class="modal-body text-center">
                      <h2 class="text-uppercase">hello super spa</h2>
                      <p class="body-message centered"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore officiis vitae aperiam esse perferendis iusto alias distinctio provident id incidunt. Eaque rerum illum quia eum tenetur molestias? Accusantium iste est fugiat quasi numquam quibusdam vero! Quibusdam facere placeat quisquam repudiandae. </p>
                      <span>£12.00</span>
                    </div>
  
                  </div>                           
                </div>              
              </div><!-- Modal Content: ends -->

           <!-- // ##############
          // Product 5
          // ############## -->
          <div class="col-6 col-md-3 product-wrapper">
              <div class="image-wrapper">
                <a data-toggle="modal" data-target=".product-1" href="#"><img class="img-fluid" src="img/products/product-5.png" alt="Try your luxe"></a>
              </div>
              <div class="text-wrapper text-center text-uppercase">
                <div class="title ">
                  try your lux
                </div>
                <div class="view-more">
                  <a data-toggle="modal" data-target=".product-1" href="#">view more details <span class="caret-black">></span></a>
                </div>
                <div class="add-to-basket">
                  <p>add to basket <span class="caret-black">></span></p> 
                </div>
              </div>
            </div>
  
            <!-- Modal Content: begins -->
            <div class="modal fade product-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                                 
                  <div class="modal-content">
                    
                    <!-- Modal Header -->
                    <div class="modal-header no-borders">
                        <button type="button" class="close" data-dismiss="modal"><img src="img/main/close-button.png" alt=""></button>
                        <h4 class="modal-title" id="gridSystemModalLabel"></h4>
                    </div>   
                    <!-- Modal Body -->  
                    <div class="modal-body text-center">
                      <h2 class="text-uppercase">TRY YOUR LUXE</h2>
                      <p class="body-message centered"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore officiis vitae aperiam esse perferendis iusto alias distinctio provident id incidunt. Eaque rerum illum quia eum tenetur molestias? Accusantium iste est fugiat quasi numquam quibusdam vero! Quibusdam facere placeat quisquam repudiandae. </p>
                      <span>£12.00</span>
                    </div>
  
                  </div>                           
                </div>              
              </div><!-- Modal Content: ends -->


          <!-- // ##############
          // Product 6
          // ############## -->
          <div class="col-6 col-md-3 product-wrapper">
              <div class="image-wrapper">
                <a data-toggle="modal" data-target=".product-1" href="#"><img class="img-fluid" src="img/products/product-6.png" alt="Try your luxe"></a>
              </div>
              <div class="text-wrapper text-center text-uppercase">
                <div class="title ">
                  try your lux
                </div>
                <div class="view-more">
                  <a data-toggle="modal" data-target=".product-1" href="#">view more details <span class="caret-black">></span></a>
                </div>
                <div class="add-to-basket">
                  <p>add to basket <span class="caret-black">></span></p> 
                </div>
              </div>
            </div>
  
            <!-- Modal Content: begins -->
            <div class="modal fade product-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                                 
                  <div class="modal-content">
                    
                    <!-- Modal Header -->
                    <div class="modal-header no-borders">
                        <button type="button" class="close" data-dismiss="modal"><img src="img/main/close-button.png" alt=""></button>
                        <h4 class="modal-title" id="gridSystemModalLabel"></h4>
                    </div>   
                    <!-- Modal Body -->  
                    <div class="modal-body text-center">
                      <h2 class="text-uppercase">TRY YOUR LUXE</h2>
                      <p class="body-message centered"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore officiis vitae aperiam esse perferendis iusto alias distinctio provident id incidunt. Eaque rerum illum quia eum tenetur molestias? Accusantium iste est fugiat quasi numquam quibusdam vero! Quibusdam facere placeat quisquam repudiandae. </p>
                      <span>£12.00</span>
                    </div>
  
                  </div>                           
                </div>              
              </div><!-- Modal Content: ends -->



          <!-- // ##############
          // Product 7
          // ############## -->
          <div class="col-6 col-md-3 product-wrapper">
              <div class="image-wrapper">
                <a data-toggle="modal" data-target=".product-1" href="#"><img class="img-fluid" src="img/products/product-7.png" alt="Try your luxe"></a>
              </div>
              <div class="text-wrapper text-center text-uppercase">
                <div class="title ">
                  try your lux
                </div>
                <div class="view-more">
                  <a data-toggle="modal" data-target=".product-1" href="#">view more details <span class="caret-black">></span></a>
                </div>
                <div class="add-to-basket">
                  <p>add to basket <span class="caret-black">></span></p> 
                </div>
              </div>
            </div>
  
            <!-- Modal Content: begins -->
            <div class="modal fade product-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                                 
                  <div class="modal-content">
                    
                    <!-- Modal Header -->
                    <div class="modal-header no-borders">
                        <button type="button" class="close" data-dismiss="modal"><img src="img/main/close-button.png" alt=""></button>
                        <h4 class="modal-title" id="gridSystemModalLabel"></h4>
                    </div>   
                    <!-- Modal Body -->  
                    <div class="modal-body text-center">
                      <h2 class="text-uppercase">TRY YOUR LUXE</h2>
                      <p class="body-message centered"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore officiis vitae aperiam esse perferendis iusto alias distinctio provident id incidunt. Eaque rerum illum quia eum tenetur molestias? Accusantium iste est fugiat quasi numquam quibusdam vero! Quibusdam facere placeat quisquam repudiandae. </p>
                      <span>£12.00</span>
                    </div>
  
                  </div>                           
                </div>              
              </div><!-- Modal Content: ends -->

          <!-- // ##############
          // Product 8
          // ############## -->
          <div class="col-6 col-md-3 product-wrapper">
              <div class="image-wrapper">
                <a data-toggle="modal" data-target=".product-1" href="#"><img class="img-fluid" src="img/products/product-8.png" alt="Try your luxe"></a>
              </div>
              <div class="text-wrapper text-center text-uppercase">
                <div class="title ">
                  try your lux
                </div>
                <div class="view-more">
                  <a data-toggle="modal" data-target=".product-1" href="#">view more details <span class="caret-black">></span></a>
                </div>
                <div class="add-to-basket">
                  <p>add to basket <span class="caret-black">></span></p> 
                </div>
              </div>
            </div>
  
            <!-- Modal Content: begins -->
            <div class="modal fade product-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                                 
                  <div class="modal-content">
                    
                    <!-- Modal Header -->
                    <div class="modal-header no-borders">
                        <button type="button" class="close" data-dismiss="modal"><img src="img/main/close-button.png" alt=""></button>
                        <h4 class="modal-title" id="gridSystemModalLabel"></h4>
                    </div>   
                    <!-- Modal Body -->  
                    <div class="modal-body text-center">
                      <h2 class="text-uppercase">TRY YOUR LUXE</h2>
                      <p class="body-message centered"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore officiis vitae aperiam esse perferendis iusto alias distinctio provident id incidunt. Eaque rerum illum quia eum tenetur molestias? Accusantium iste est fugiat quasi numquam quibusdam vero! Quibusdam facere placeat quisquam repudiandae. </p>
                      <span>£12.00</span>
                    </div>
  
                  </div>                           
                </div>              
              </div><!-- Modal Content: ends -->

        </div> <!-- Row End -->
    </div> <!-- Container End -->

  </section>


  <footer>
      <div class="footer-content">
          <img class="img-fluid" src="img/main/footer-logo.png" alt="Glam Todings Logo">
      </div>
  </footer>

  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
  <script src="js/main.js"></script>
</body>
</html>