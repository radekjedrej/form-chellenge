<?php
   
    require_once 'partials/database.php'; 
   
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="x-ua-compatible" content="ie=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <link rel="stylesheet" href="css/style.css">
        <title></title>
    </head>
<body>
  
  <section class="festive-fun">
    <div class="container">

    <div class="row justify-content-center">
        <div class="col-12">
            <div class="header-festive-fun text-center pb-5">
                <img class="img-fluid" src="img/main/header-festive-fun.png" alt="It's Winter">
            </div>

            <!-- Form Start  -->
            <div class="main-form-group">
                <form id="myForm" action="partials/save.php" method="POST"> 

                    <div class="form-group row"> 
                        <label for="first-name" class="col-4 col-sm-3 col-lg-2 col-form-label text-uppercase">First Name<sup>*</sup></label>
                        <div class="col-8 col-sm-6">
                            <!-- If Someone turn off Javascript And Submit form with errors, like empty fields, Values are not going to delete! I Did only one for Demo -->
                            <?php if(isset($_GET['first'])) { ?>
                            <input class="form-control my-inputs" type="text" name="first_name" value="<?php echo $first ?>" id="first-name" required>
                            <?php } else { ?>
                            <input class="form-control my-inputs" type="text" name="first_name" value="" id="first-name" required>
                            <?php } ?>
                            <!-- -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="last-name" class="col-4 col-sm-3 col-lg-2 col-form-label text-uppercase">last Name<sup>*</sup></label>
                        <div class="col-8 col-sm-6">
                            <input class="form-control my-inputs" type="text" name="last_name" value="" id="last-name" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-4 col-sm-3 col-lg-2 col-form-label text-uppercase">Email Address<sup>*</sup></label>
                        <div class="col-8 col-sm-6">
                            <input class="form-control my-inputs" type="email" name="email" value="" id="email" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="city" class="col-4 col-sm-3 col-lg-2 col-form-label text-uppercase">city<sup>*</sup></label>
                        <div class="col-8 col-sm-6">
                            <input class="form-control my-inputs" type="text" value="" name="city" id="city" required>
                        </div>
                    </div>
                    
                    <!-- Start Select DOB -->
                    <div class="form-group row"> 
                    <label for="dob" class="col-4 col-sm-3 col-lg-2 col-form-label text-uppercase">Date of Birth</label>
                    <div class="col-2">
                        <select name="birth_day" class="custom-select" required>
                        <!-- Start Select Day -->
                        <option value="">DAY</option>
                            <?php 
                                $start_date = 1;
                                $end_date   = 31;
                                for( $j=$start_date; $j<=$end_date; $j++ ) {
                                    echo '<option value='.$j.'>'.$j.'</option>';
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col-2">
                        <select name="birth_month" class="custom-select" required>
                            <!-- Start Select Month -->
                            <option value="">MONTH</option>
                                <?php for( $m=1; $m<=12; ++$m ) { 
                                $month_label = date('F', mktime(0, 0, 0, $m, 1));
                                ?>
                                <option value="<?php echo $m; ?>"><?php echo $month_label; ?></option>
                                <?php } ?>
                        </select>
                    </div>
                    <div class="col-2">

                        <select name="birth_year" class="custom-select" required>
                            <!-- Start Select Year -->
                            <option value="">YEAR</option>
                            <?php 
                                $year = date('Y');
                                $min = $year - 60;
                                $max = $year;
                                for( $i=$max; $i>=$min; $i-- ) {
                                    echo '<option value='.$i.'>'.$i.'</option>';
                                }
                            ?>
                        </select>
                    </div>
                    </div> 
                    <!-- End Select DOB -->
                    
                    
                    <div class="form-group row">
                    <label for="country" class="col-4 col-sm-3 col-lg-2 col-form-label text-uppercase align-middle">COUNTRY<sup>*</sup></label>
                    <div class="col-8 col-sm-6">
                        <input class="form-control my-inputs" type="text" value="" name="country" id="country" required>
                    </div>
                    <div class="invalid-feedback">Example invalid custom select feedback</div>
                    </div>

                    <!-- Checkboxes Group 1 Start-->
                    <div class="categories-remember-heading">
                        <p class="text-uppercase cat-title">Which of the below Soap &amp; Glory categories do you remember seeing in store?<sup>*</sup></p>
                        <p class="cat-sub">Please select as many as appropriate</p>
                    </div>

                    <div class="groupa">
                        <div class="form-check">
                        <label class="custom-control custom-checkbox">
                            <input name="categories_remember[]" value="facial_skincare" type="checkbox" class="group-a custom-control-input" required>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Facial skincare</span>
                        </label>
                        </div>

                        <div class="form-check">
                        <label class="custom-control custom-checkbox">
                            <input name="categories_remember[]" value="cosmetics" type="checkbox" class="group-a custom-control-input" required>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Cosmetics</span>
                        </label>
                        </div>

                        <div class="form-check">
                        <label class="custom-control custom-checkbox">
                            <input name="categories_remember[]" value="bath_body" type="checkbox" class="group-a custom-control-input" required>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Bath &amp; Body</span>
                        </label>
                        </div>

                        <div class="form-check">
                        <label class="custom-control custom-checkbox">
                            <input name="categories_remember[]" value="nail_polishes" type="checkbox" class="group-a custom-control-input" required>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Nail polishes</span>
                        </label>
                        </div>

                        <div class="form-check">
                        <label class="custom-control custom-checkbox">
                            <input name="categories_remember[]" value="bathing_accessories" type="checkbox" class="group-a custom-control-input" required>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Bathing accessories (washbags, exfoliating gloves etc.)</span>
                        </label>
                        </div>

                        <div class="form-check">
                        <label class="custom-control custom-checkbox">
                            <input name="categories_remember[]" value="haircare" type="checkbox" class="group-a custom-control-input" required>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Haircare</span>
                        </label>
                        </div>
                    </div>
                    <!-- Checkboxes Group 1 End-->

                    <!-- Checkboxes Group 2 Start-->
                    <div class="categories-consider-heading">
                    <p class="text-uppercase cat-title">Which of the below Soap &amp; Glory categories would you consider buying?<sup>*</sup></p>
                    <p class="cat-sub">Please select as many as appropriate</p>
                    </div>
                    
                    <div class="groupb">
                        <div class="form-check">
                        <label class="custom-control custom-checkbox">
                            <input name="categories_consider[]" value="facial_skincare" type="checkbox" class="group-b custom-control-input" required>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Facial skincare</span>
                        </label>
                        </div>

                        <div class="form-check">
                        <label class="custom-control custom-checkbox">
                            <input name="categories_consider[]" value="cosmetics" type="checkbox" class="group-b custom-control-input" required>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Cosmetics</span>
                        </label>
                        </div>

                        <div class="form-check">
                        <label class="custom-control custom-checkbox">
                            <input name="categories_consider[]" value="bath_body" type="checkbox" class="group-b custom-control-input" required>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Bath &amp; Body</span>
                        </label>
                        </div>

                        <div class="form-check">
                        <label class="custom-control custom-checkbox">
                            <input name="categories_consider[]" value="nail_polishes" type="checkbox" class="group-b custom-control-input" required>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Nail polishes</span>
                        </label>
                        </div>

                        <div class="form-check">
                        <label class="custom-control custom-checkbox">
                            <input name="categories_consider[]" value="bathing_accessories" type="checkbox" class="group-b custom-control-input" required>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Bathing accessories (washbags, exfoliating gloves etc.)</span>
                        </label>
                        </div>

                        <div class="form-check">
                        <label class="custom-control custom-checkbox">
                            <input name="categories_consider[]" value="haircare" type="checkbox" class="group-b custom-control-input"  required>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Haircare</span>
                        </label>
                        </div>
                    </div>
                    <!-- Checkboxes Group 2 End-->

                    <!-- Checkboxes Group 3 Start-->
                    
                    <div class="groupc">
                        <div class="statements-heading">
                        <p class="text-uppercase cat-title">Which of the following statements applies to you? Please select as many as appropriate*<sup>*</sup></p>
                        <p class="cat-sub">(Please be honest! Your chance of winning is the same no matter what your answer.)</p>
                        </div>

                                
                        <div class="form-check">
                        <label class="custom-control custom-checkbox">
                            <input name="statements_applies[]" value="never_tried" class="group-c custom-control-input" type="checkbox" required>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">I’ve never tried Soap &amp; Glory products before receiving SPA OF WONDER<sup>&trade;</sup></span>
                        </label>
                        </div>

                        <div class="form-check">
                        <label class="custom-control custom-checkbox">
                            <input name="statements_applies[]" value="every_christmas" class="group-c custom-control-input" type="checkbox" required>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">I expect to receive Soap &amp; Glory products every Christmas</span>
                        </label>
                        </div>

                        <div class="form-check">
                        <label class="custom-control custom-checkbox">
                            <input name="statements_applies[]" value="not_chrismas" class="group-c custom-control-input" type="checkbox" required>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Soap &amp; Glory is not typically a gift I receive at Christmas</span>
                        </label>
                        </div>

                        <div class="form-check">
                        <label class="custom-control custom-checkbox">
                            <input name="statements_applies[]" value="plan_to_buy" class="group-c custom-control-input" type="checkbox" required>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">I plan to buy Soap &amp; Glory products for myself in the next three months</span>
                        </label>
                        </div>
                    </div>
                    <!-- Checkboxes Group 3 End-->

                    <!-- Checkboxes Group 4 Start-->
                    <div class="keeping-box-heading">
                    <p class="text-uppercase cat-title">Do you intend on keeping the box?*<sup>*</sup></p>
                    </div>

                    <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="keep_box" id="keep_box_1" value="1" checked>
                        Yes
                    </label>
                    </div>
                    <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="keep_box" id="Keep_box_2" value="0">
                        No
                    </label>
                    </div>
                    <!-- Checkboxes Group 4 End-->

                    <!-- Textarea Group 5 Start-->

                    <div class="form-group">
                    <p class="text-uppercase cat-title">What are you planning on doing with it?</p>
                    <textarea name="planing_todo" class="form-control" id="planing_todo" rows="3"></textarea>
                    </div>

                    <!-- Textarea Group 5 End-->

                    <!-- Checkboxes Group 6 Start-->

                    <div class="form-check">
                    <label class="custom-control custom-checkbox">
                        <input name="news_offers" value="1" type="checkbox" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description text-inherit">We’d like to keep you up to date with news, offers and free stuff, including occasional offers from other companies in our group. Don’t worry – we won’t spam you and you can opt out at any time! You can have a look at our Privacy Policy here.</span>
                    </label>
                    </div>

                    <div class="form-check">
                    <label class="custom-control custom-checkbox">
                        <input name="terms_condition" value="1" type="checkbox" class="custom-control-input" required>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description text-inherit">I agree to the Terms and Conditions</span>
                    </label>
                    </div>
                    <!-- Checkboxes Group 6 End-->


                    
                    <div class="button-submit">
                        <button  id="checkBtn" type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </div>

                    <?php

                    if(!isset($_GET['signup'])) {                       
                    } else {
                        $signupCheck = $_GET['signup'];
                        
                        if($signupCheck == 'empty'){
                            echo "<p class='alert-danger'>You did not fill all fields</p>";
                            exit();
                        }
                        elseif($signupCheck == 'invalid'){
                            echo "<p class='alert-danger'>You used invalid characters</p>";
                            exit();
                        }
                        elseif($signupCheck == 'success'){
                            echo "<p class='alert-success'>Form been submited!</p>";
                            exit();
                        }
                    }
                    ?>

                </form> <!-- Form Ends -->
            </div>

        </div>
    </div>


    </div>
  </section>

    <footer> <!-- Footer Start -->
        <div class="footer-content">
            <img class="img-fluid" src="img/main/footer-logo.png" alt="Glam Todings Logo">
        </div>
    </footer> <!-- Footer End -->

                                 
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/warning.js"></script>
    <script src="js/main.js"></script>
</body>
</html>