<?php

  require_once 'config.php';

  // Variables
  $error;
 
  // Set DSN
  $dsn = 'mysql:host=' . $host . ';dbname=' . $dbname;
  $options = array(
    PDO::ATTR_PERSISTENT => true,
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
  );

  // Create PDO Instance
  try{
    $pdo = new PDO($dsn, $user, $password);
  } catch(PDOException $e) {
    $error = $e->getMessage();
    echo $error;
  }


