<?php



if(isset($_POST['submit'])) {

  include_once 'database.php';

  // All Form Data
  $firstName = mysqli_real_escape_string($conn, $_POST['first_name']);
  $lastName = mysqli_real_escape_string($conn, $_POST['last_name']);
  $email = mysqli_real_escape_string($conn, $_POST['email']);
  $city = mysqli_real_escape_string($conn, $_POST['city']);
  $birth_day = mysqli_real_escape_string($conn, $_POST['birth_day']);
  $birth_month = mysqli_real_escape_string($conn, $_POST['birth_month']);
  $birth_year = mysqli_real_escape_string($conn, $_POST['birth_year']);
  $country = mysqli_real_escape_string($conn, $_POST['country']);
  $categories_remember = (implode(',', $_POST['categories_remember']));
  $categories_consider = (implode(',', $_POST['categories_consider']));
  $statements_applies = (implode(',', $_POST['statements_applies']));
  $keep_box = mysqli_real_escape_string($conn, $_POST['keep_box']);
  $planing_todo = mysqli_real_escape_string($conn, $_POST['planing_todo']);
  $news_offers = mysqli_real_escape_string($conn, $_POST['news_offers']);
  $terms_condition = mysqli_real_escape_string($conn, $_POST['terms_condition']);

  // Combine Year Month Day Into Date Format
  $date = $birth_year . '-' . $birth_month . '-' . $birth_day;
  
  // Error handlers
  // Check for mpty fields
  if(empty($firstName) || empty($lastName) || empty($email) || empty($city) || empty($date) || empty($country) || empty($categories_remember) || empty($categories_consider) || empty($statements_applies) || empty($terms_condition)) {
      header('Location: ../festive-fun.php?signup=empty');
      exit();
  } else {
    // Check if input variables are valid
    if(!preg_match("/^[a-zA-Z]*$/", $firstName) || !preg_match("/^[a-zA-Z]*$/", $lastName) || !preg_match("/^[a-zA-Z]*$/", $city) || !preg_match("/^[a-zA-Z]*$/", $country) || !preg_match("/[A-Za-z0-9]+/", $planing_todo)) {
      header('Location: ../festive-fun.php?signup=invalid');
      exit();      
    } else {
      // Check if email is valid
      if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        header('Location: ../festive-fun.php?signup=email&first=$firstName&last=$lastName&email=$email&city=$city&country=$country');
        exit();              
      } else {
        // Insert data into database
        $sql = "INSERT INTO test_two (first_name, last_name, email, city, dob, country, categories_remember, categories_consider, select_appropriate, keeping_box, text, subscribe, conditions) VALUES ('$firstName', '$lastName', '$email', '$city', '$date', '$country', '$categories_remember', '$categories_consider', '$statements_applies', '$keep_box', '$planing_todo', '$news_offers', '$terms_condition');";
        mysqli_query($conn, $sql);
        header('Location: ../festive-fun.php?signup=success');
        exit(); 
      }
    }
  }

} else {
    header('Location: ../festive-fun.php');
    exit();
}
  